# Assemblage des modules

![Circuit imprimé](img/PCB.png)

## Carte à microcontrolleur

![Pièces à souder](img/Xiao_0.jpg) ![Assemblage avant soudure](img/Xiao_1.jpg)

Pour cette carte, l'alignement des connecteurs ne pose pas de problème car les trous ont exactement le bon diamètre pour que la barette soit parfaitement droite.

## Capteur de CO2

![Pièces à souder](img/Sensor_0.jpg)

![Alignement des pièces](img/Sensor_1.jpg)

![Pièces soudées](img/Sensor_2.jpg)

## Module LED

Pour le module LED, prévoyez un câble à 3 fils d'environ 5cm de long.

![Pièces à souder](img/Wiring_0.jpg)

![Positionnement des fils](img/Wiring_1.jpg)

![Pièces soudées](img/Wiring_2.jpg)

Essayez de couper l'excédent de soudure le plus bas possible, pour éviter d'ajouter trop d'épaisseur au module.

# Assemblage du circuit

## Nettoyage

Pour un circuit gravé à la fraiseuse, il est indispensable de nettoyer la surface avec du papier de verre car le cuivre s'oxyde naturellement et cette couche d'oxyde empêche la soudure entre l'étain et le cuivre.

Cette opération doit être réalisée juste avant de commencer à souder.

![Circuit brut](img/PCB_raw.jpg)

![Circuit nettoyé](img/PCB_clean.jpg)

Si le circuit a été gravé par une méthode chimique, il faudra nettoyer le vernis protecteur à l'acétone.

De nouveau, l'opération doit être réalisée juste avant de commencer à souder pour éviter l'oxydation du cuivre.

## Connecteurs

Pour un alignement optimal des connecteurs, on peut y embrocher la carte à microcontrolleur (ou une barrette de pins).

![Alignement des connecteurs](img/Pin_0.jpg)

![Alignement des connecteurs](img/Pin_1.jpg)

Après avoir soudé au moins une pin par connecteur, on peut enlever la pièce qui servait de repère.

Ainsi, ce sera plus facile de terminer les soudures.

![Pièces soudées](img/Pin_2.jpg)

Ici aussi, on peut utiliser une barrette de pins pour s'assurer du bon alignement des connecteurs.

![Alignement des connecteurs](img/Pin_3.jpg) ![Pièces soudées](img/Pin_4.jpg)

![Pièces soudées](img/Pin_5.jpg)

![Pièces soudées](img/Pin_6.jpg)

## Diode

![Positionnement de la diode](img/Diode_1.jpg)

![Blocage de la diode](img/Diode_2.jpg)

![Soudure de la diode](img/Diode_3.jpg)

![Coupe de l'excédent](img/Diode_4.jpg)

![Résultat de la soudure](img/Diode_5.jpg)

## Finalisation

![Pièces à assembler](img/Assembly_0.jpg)

![Circuit assemblé](img/Assembly_1.jpg)

Le câblage est décrit dans la section [Assemblage](Assembly.md).
