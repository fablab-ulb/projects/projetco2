# Assemblage du boîtier

Pour l'assemblage, on utilisera 12 vis M3 de 6mm de long.

Les trous doivent donc être taraudés pour accueillir un pas de vis M3.

Si vous n'avez pas d'outil pour le taraudage, vous pouvez utiliser une vis en métal.

![Taraudage avec une vis](img/Assembly_2.jpg)

## Face arrière

Taraudez les 4 trous et vissez le circuit.

![Assemblage du circuit sur la face arrière](img/Assembly_3.jpg)

## Câblage

![Connecteurs à câbler](img/Pinout.jpg)

Pour l'afficheur à 4 digits, prévoyez un câble à 4 fils d'environ 10cm de long.

![Câblage de l'afficheur](img/Wiring_3.jpg)

## Face avant

Emboitez les modules LED sur leurs emplacements. Attention à ne pas casser les supports !

![Insertion de l'afficheur](img/Assembly_4.jpg)

Ici, j'ai ajouté une fine feuille de plastique translucide blanc qui servira de diffuseur pour la LED.

![Insertion de la LED](img/Assembly_5.jpg)

Pour fixer les modules sur la face avant, j'ai choisi de les souder avec le plastique du boîtier.

Prenez un objet en métal (ici, une clé allen de 2mm) et chauffez-le à la flamme d'un briquet.

![Chauffe de l'outil](img/Assembly_6.jpg)

Ensuite, pressez le métal chaud dans le trou du module.

![Thermoformage](img/Assembly_7.jpg)

Répétez l'opération pour chaque trou. Si la soudure est bien faite, le module tient en place.

![Module soudé à la face avant](img/Assembly_8.jpg)
