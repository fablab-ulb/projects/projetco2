# Circuit imprimé

![Circuit imprimé](img/PCB.png)

Vous pouvez télécharger les fichiers EAGLE via les liens suivants :

* Dessin : [projetCO2.brd](files/projetCO2.brd)
* Schéma : [projetCO2.sch](files/projetCO2.sch)
* Les deux fichiers dans un dossier ZIP : [EAGLE_files.zip](files/EAGLE_files.zip)

## Gravure CNC

Le circuit peut être gravé à la fraiseuse CNC avec une fraise de 0.6mm de diamètre.

## Gravure chimique

Pour la gravure chimique, on a besoin d'un masque pour l'insolation aux UV.

Vous pouvez imprimer le fichier [PCB_mask.pdf](files/PCB_mask.pdf) sur un transparent A4.

Faites bien attention à imprimer le document à l'échelle 1:1 (taille réelle) !

Pour l'insolation de la carte, placez le transparent de façon à ce que l'encre soit contre le cuivre.
