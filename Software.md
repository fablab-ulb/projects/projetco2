# Programmation

La programmation se fait via l'IDE arduino. Pour l'installer, il faut le télécharger sur le site [https://www.arduino.cc/en/software](https://www.arduino.cc/en/software).

## Ajout de la carte Seeeduino Xiao dans l'IDE arduino

Dans l'IDE arduino, cliquez sur l'onglet **Fichier** puis sur **Préférences**.

![Préférences de l'IDE arduino](img/URL.png)

Dans le champ **URL de gestionnaire des cartes supplémentaires**, collez l'adresse suivante :

https://files.seeedstudio.com/arduino/package_seeeduino_boards_index.json

Puis cliquez sur **OK**.

Ensuite, cliquez sur l'onglet **Outils** puis **Type de carte** et **Gestionnaire de cartes**.

Dans le champ de recherche, entrez **Seeeduino**.

![Gestionnaire de cartes](img/Boards.png)

Dans le menu déroulant, sélectionnez la dernière version et cliquez sur **Installer**.

## Installation des librairies dans l'IDE arduino

Dans l'IDE arduino, cliquez sur l'onglet **Croquis** puis **Inclure une bibliothèque** et **Gérer les bibliothèques**.

Dans le champ de recherche, entrez **TM1637**. Dans les résultats, cherchez celui qui s'appelle simplement **TM1637**. En le survolant avec le curseur, un bouton **Installer** apparaît. Cliquez dessus.

Recommencez la même opération avec les librairies **Adafruit NeoPixel** et **SparkFun SCD30**.

## Test du capteur

Avant de procéder au câblage des afficheurs, on peut tester le capteur de CO2.

Téléchargez d'abord le programme [projetCO2_test.ino](files/projetCO2_test.ino) et ouvrez-le dans l'IDE arduino.

Branchez la carte à microcontrolleur à votre PC via un câble USB.

Dans l'IDE arduino, cliquez sur l'onglet **Outils** puis **Type de carte**.

Dans la liste, cliquez sur **Seeed SAMD** puis **Seeeduino XIAO**.

Cliquez sur le bouton **Téléverser**.

![Téléverser](img/Upload.png)

Ouvrez le **moniteur série**.

![Moniteur série](img/Monitor.png)

Si tout fonctionne, la concentration en CO2 devrait s'afficher toutes les 2 secondes.

## Programme

Le programme final fonctionne comme pour le test, mais sans rien envoyer sur le port USB.

Vous pouvez le télécharger ici : [projetCO2.ino](files/projetCO2.ino)
