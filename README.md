# projetCO2

## Documentation

* [Liste des composants](Parts.md)
* [Impression 3D](3D.md)
* [Conception du circuit imprimé](PCB.md)
* [Électronique](Electronics.md)
* [Assemblage ](Assembly.md)
* [Programmation](Software.md)

## Crédits

Ce projet a été réalisé par l'[Expérimentarium de Physique](https://www.experimentarium.be/) de l'ULB, à l'aide des machines du [FabLab ULB](http://fablab-ulb.be/) et de la [Scientothèque](https://www.lascientotheque.be/).

Il est inspiré du [Projet CO2](https://projetco2.fr/) et des réalisations qui sont maintenant sur le site [Makers CO2](http://nousaerons.fr/makersco2/).
