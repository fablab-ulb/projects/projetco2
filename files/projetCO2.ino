#include <Wire.h>
#include <SparkFun_SCD30_Arduino_Library.h>
#include <Adafruit_NeoPixel.h>
#include <TM1637Display.h>

#define CLK 8
#define DIO 10

SCD30 airSensor;

Adafruit_NeoPixel pixels(1, 6, NEO_GRB);

TM1637Display display(CLK, DIO);

int ppm = 0;

void setup()
{
  display.setBrightness(7, true);

  pixels.begin();
  pixels.clear();

  Wire.begin();
  airSensor.begin();
}

void loop()
{
  if (airSensor.dataAvailable())
  {
    ppm = airSensor.getCO2();

    display.showNumberDec(ppm, false, 4, 0);

    if (ppm >= 1000)
    {
      pixels.setPixelColor(0, pixels.Color(255, 0, 0));
      pixels.show();
    }
    else if (ppm >= 800)
    {
      pixels.setPixelColor(0, pixels.Color(255, 96, 0));
      pixels.show();
    }
    else
    {
      pixels.setPixelColor(0, pixels.Color(0, 64, 0));
      pixels.show();
    }
  }
  delay(2000);
}
