# Matériel et composants électroniques

## Capteur de CO2 - SCD30

* [Mouser](https://www.mouser.be/ProductDetail/403-SCD30)
* [Electran](https://www.electan.com/co2-p-9609-en.html)
* [Antratek](https://www.antratek.be/co2-and-rh-t-sensor-scd30)
* [RS](https://befr.rs-online.com/web/p/temperature-humidity-sensor-ics/1720552)

## Carte à microcontrolleur - Seeeduino XIAO

* [Mouser](https://www.mouser.be/ProductDetail/713-102010328)
* [Antratek](https://www.antratek.be/seeeduino-xiao-samd21-cortex-m0)
* [RS](https://befr.rs-online.com/web/p/arduino-compatible-boards-kits/2005273)
* [GoTronic](https://www.gotronic.fr/art-seeeduino-xiao-samd21-102010328-31300.htm)

## Module afficheur LED à 4 digits - TM1637

* [Makershop](https://www.makershop.de/display/led-matrix/led-tm1637/)
* [Diymore](https://www.diymore.cc/products/4-bits-tm1637-digital-tube-led-clock-display-module-for-arduino-due-uno-2560-r3)
* [Christians Technikshop](https://www.christians-shop.de/TM1637-LED-Timer-Display-7-Segment-Display-in-red-4-bit)

## Module LED 3 couleurs - WS2811/WS2812

* [Electran](https://www.electan.com/sparkfun-rgb-led-breakout-ws2812b-p-6626-en.html)
* [Diymore](https://www.diymore.cc/products/new-ws2812-ws2811-rgb-led-breakout-diy-kit-electronic-pcb-board-module-for-arduino)
* [Mouser](https://www.mouser.be/ProductDetail/474-BOB-13282)

<!-- ## Color OLED module

* [Banggood](https://www.banggood.com/0_95-Inch-7pin-Full-Color-65K-Color-SSD1331-SPI-OLED-Display-p-1068167.html)
* [Diymore](https://www.diymore.cc/products/diymore-0-95-inch-7-pin-full-color-65k-ssd1331-96x64-resolution-spi-oled-display-module-for-arduino) -->

## Connecteur femelle 4 pins

* [GoTronic](https://www.gotronic.fr/art-connecteur-fh1x4-22730.htm)
* [RS](https://befr.rs-online.com/web/p/pcb-sockets/1557896)
* [MCHobby](https://shop.mchobby.be/fr/conn/612-connecteur-femelle-1x4-broches-3232100006126.html)

## Connecteur femelle 7 pins

* [GoTronic](https://www.gotronic.fr/art-connecteur-fh1x7-22733.htm)
* [RS](https://befr.rs-online.com/web/p/pcb-sockets/1548327)
* [MCHobby](https://shop.mchobby.be/fr/conn/2080-connecteur-femelle-1x7-broches-3232100020801.html)

## Connecteur mâle

* [GoTronic](https://www.gotronic.fr/art-connecteur-he14-mh100-4457.htm)
* [MCHobby](https://shop.mchobby.be/fr/conn/76-2-x-36-pin-header-normal-3232100000766.html)

Pour faciliter le câblage, j'ai utilisé des connecteurs de couleurs différentes.

Vous pouvez trouver ces couleurs chez les même revendeurs :

* [GoTronic](https://www.gotronic.fr/cat-connecteurs-secables-he14-894.htm)
* [MCHobby](https://shop.mchobby.be/fr/42-conn)

## Carte de cuivre FR1/FR2 pour fraisage CNC

* [RS](https://befr.rs-online.com/web/p/plain-copper-ink-resist-boards/0433927)
* [GoTronic](https://www.gotronic.fr/art-bakelite-presensibilisee-6980.htm)

## Câble USB-C

* [RS](https://befr.rs-online.com/web/p/usb-cables/1863055)
* [GoTronic](https://www.gotronic.fr/art-cordon-2-m-usbc-2m-33655.htm)

## Transformateur

* [RS](https://befr.rs-online.com/web/p/ac-dc-adapters/1217114)
* [GoTronic](https://www.gotronic.fr/art-alimentation-psusb-wn-8213.htm)

## Visserie

L'assemblage nécéssite 12 vis M3 de 6mm de long.

* [RS](https://befr.rs-online.com/web/p/machine-screws/1854419)
* [GoTronic](https://www.gotronic.fr/art-jeu-de-10-vis-nylon-6-mm-20973.htm)
* [Christians Technikshop](https://www.christians-shop.de/M3-Nylon-Kit-180er-Set-with-screws-spacers-nuts)

Le dernier lien concerne un kit de vis, écrous et entretoises. Ce kit contient 60 vis M3 x 6mm.

## Câblage

Une solution facile est d'acheter des câbles assemblés, par exemple chez Antratek :

* [3-pin](https://www.antratek.com/3-pin-10-extension-cable)
* [4-pin](https://www.antratek.com/jumper-wire-0-1-4-pin-12)

Mais ils sont relativement chers, et surtout trop longs.

La meilleure solution serait de couper les câbles sur mesure et de les sertir soi-même. Pour cela, une pince à sertir est nécéssaire, ainsi que le matériel ci-dessous.

### Nappe de câbles femelle-femelle

* [Electran](https://www.electan.com/pin-femalefemale-splittable-jumper-wire-200mm-p-6287-en.html)
* [Christians Technikshop](https://www.christians-shop.de/40x-Jumper-Wire-254-mm-socket-on-socket-cable-plug-bridge-breadboard-Christians-Technikshop)

### Pince à sertir

* [GoTronic](https://www.gotronic.fr/art-pince-a-sertir-ht225d-7126.htm)
* [MCHobby](https://shop.mchobby.be/fr/outillage/1339-pince-a-sertir-pour-fil-008-a-05mm-3232100013391.html)

### Connecteurs à sertir

* [Christians Technikshop](https://www.christians-shop.de/620-piece-Set-DuPont-plug-in-connections-Plugs-Sockets-Jumper-1x1-to-2x6-Pin-incl-female-and-male-crimp-connector-Christians-Technikshop)
* [Diymore](https://www.diymore.cc/products/620pcs-jumper-dupont-wire-cable-header-connector-housing-kit-male-female-box)
